L1 = 1.5;
L2 = 4.0;
L3 = 2.0 * L2 + L1;
D1 = 4.0;
H1 = 0.8;
H2 = 4.0;

module right_circular(){
    difference(){
        translate([0, 0, 0.5 * H2]) cube([L3, D1, H2], center = true);
        translate([0, 0, 0.5 * 99.9 + H1 + 0.5 * L1]) cube([L1, 99.9, 99.9], center = true);
        translate([0, 0, 0.5 * L1 + H1]) rotate([90, 0, 0]) cylinder(99.9, d = L1, center = true, $fn = 100);
    }
}

right_circular();