L1 = 1.5;
L2 = 4.0;
L3 = 2.0 * L2 + L1;
D1 = 4.0;
H1 = 0.8;
H2 = 0.4;
H3 = 4.0;
d1 = 0.5;

module hourglass(){
    difference(){
        translate([0, 0, 0.5 * H3]) cube([L3, D1, H3], center = true);
        translate([0, 0, 0.5 * 99.9 + H1]) cube([L1, 99.9, 99.9], center = true);
    }
    translate([-0.5 * L1, 0, 0.5 * H2 + H1]) scale([L1 - d1, D1, 1.0]) cylinder(H2, d = 1.0, center = true, $fn = 100);
    translate([0.5 * L1, 0, 0.5 * H2 + H1]) scale([L1 - d1, D1, 1.0]) cylinder(H2, d = 1.0, center = true, $fn = 100);
}

a1 = atan((H3 - (H1 + H2)) / L2);

module negative(){
    difference(){
        rotate([0, -a1, 0]) translate([0.5 * 99.9, 0, 0.5 * 99.9]) cube([99.9, 99.9, 99.9], center = true);
        translate([-0.5 * 99.9 - 0.5 * L1, 0, 0]) cube([99.9, 99.9, 99.9], center = true);
    }
}

difference(){
    translate([-0.5 * L1, 0, -(H1 + H2)]) hourglass();
    negative();
}
