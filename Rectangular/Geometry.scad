L1 = 1.5;
L2 = 4.0;
L3 = 2.0 * L2 + L1;
D1 = 4.0;
H1 = 0.8;
H2 = 4.0;

module rectangle(){
    difference(){
        translate([0, 0, 0.5 * H2]) cube([L3, D1, H2], center = true);
        translate([0, 0, 0.5 * 99.9 + H1]) cube([L1, 99.9, 99.9], center = true);
    }
}

rectangle();